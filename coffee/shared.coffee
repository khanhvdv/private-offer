### Magic! DO NOT TOUCH! ###
###
 zoom box
###
$('#max-image').elevateZoom ->
	gallery:'gal', 
	cursor: 'pointer', 
	galleryActiveClass: 'active',
	zoomType: 'window',
	zoomWindowWidth: 350,
	zoomWindowHeight: 350,
	scrollZoom: true,
	easing: true,

###
fixed el on scroll
###
tPos = 0
header = $('header');
$mainBtn = $('.box-action .pri-btn');
$ctNav = $('.content-context')
$(window).scroll ->
	tPos = $(this).scrollTop()
	btnPos = $mainBtn.offset().top
	navPos = $ctNav.offset().top
	if tPos >= 100
		header.addClass 'fixed'
	else
		header.removeClass 'fixed'

	if tPos >= btnPos
		header.addClass 'show-btn'
	else
		header.removeClass 'show-btn'

	if tPos >= navPos
		navPos = tPos
		$ctNav
			.find '.content-nav'
			.addClass 'fixed'
	else
		$ctNav
			.find '.content-nav'
			.removeClass 'fixed'

###
animate window
###
$('a.do-animate').each ->
	$this = $(this)

	$this.click ->
		href = $this.prop('href').split('#')[1]
		$('html,body').animate({
            scrollTop: $("#"+href).offset().top},
            'slow');
		return false

###
active nav
###
$nav = $('.content-nav li')
$nav.each ->
	$(this).find('a').click ->
		$parent = $(this).closest('li')
		if !$parent.hasClass 'active'
			$parent
				.addClass 'active'
				.siblings 'li'
				.removeClass 'active'


###
tool tip
###
$('.help').tooltip();

###
running clock
###
$time = $('.time')
$day = $time.find '.day'
$hour = $time.find '.hour'
$minute = $time.find '.minute'
$second = $time.find '.second'
countDownDate = new Date(2017,11,31,12,0,0,0).getTime();
x = setInterval(->
		now = new Date().getTime()
		distance = countDownDate - now
		days = Math.floor(distance / (1000 * 60 * 60 * 24))
		hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
		minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
		seconds = Math.floor((distance % (1000 * 60)) / 1000)
		if days <= 9
			days = '0' + days
		if hours <= 9
			hours = '0' + hours
		if minutes <= 9
			minutes = '0' + minutes
		if seconds <= 9
			seconds = '0' + seconds
		$day.text(days)
		$hour.text(hours)
		$minute.text(minutes)
		$second.text(seconds)

		if distance < 0
			clearInterval x
	, 1000)

###
increase value
###
$items = $('.quantity-select')
$up = $items.find '.add-qty'
$down = $items.find '.sub-qty'
$input = $items.find '.inpt-qty'

$input.keyup ->
	if $(this).val() <= 0
		$(this).val(1)

$down.click ->
	setVal('down')

$up.click ->
	setVal('up')

setVal = (val) ->
	$curr = $input.val();
	
	if val == 'down'
		$curr--
	else if val == 'up'
		$curr++

	if $curr > 0 && $curr < 9999
		$input.val $curr


###
active color
###
$('.color-pick li input:radio').change ->
	$(this)
		.parent 'li'
		.addClass 'active'
		.siblings 'li'
		.removeClass 'active'
